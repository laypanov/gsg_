from openpyxl import load_workbook
import pymssql
from datetime import datetime


class CreateExcel(object):
    def __init__(self):
        self.wb = load_workbook('template.xlsx')
        self.dest = "result.xlsx"
        self.ws = self.wb.active

    def write_workbook(self, cell, value):
        pass

    def save_excel(self):
        self.wb.save(self.dest)

    def create_excel_data(self, sqldata, tip):
        ln_nul = '0,00'
        saldo1_ou = '0,00'
        saldo1_skl = '0,00'
        saldo2_ou = '0,00'
        saldo2_skl = '0,00'

        ln_saldo1_ou = '0,00'
        ln_saldo1_skl = '0,00'

        s_sum_skl = '0,00'
        s_rasxod_skl = '0,00'
        nomdoc1 = '0,00'
        pr_rasx = '0,00'
        skl_prix = 1
        sum_skl_prixod = 0
        sum_skl_rashod = 0

        for key, val in sqldata.items():
            for v in val:
                kolvo_prix = v['Kolvo_Prix']
                document = v.get("DOCUMENT")
                sch_doc = v.get("sch_doc")
                comment = v.get("COMMENT")
                member_nm = v.get("member_nm")
                podr = SqlData().get_podr_id(v.get("podr_nm").rstrip())
                kolvo_skl = v.get("Kolvo_Skl")


                if key == 0:  # Skl_DocsLstSvzM_Docs01 документы прихода
                    v['TIPDOC'] = '01'
                elif key == 1:  # Skl_DocsLstSvzM_Docs02 документы продажа
                    if v['TIPDOC'] == '02':
                        nomdoc1 = v.get('Nomdoc_Sch')
                elif key == 2:  # Skl_DocsLstSvzM_Docs документы 05,06,07,08
                    pass
                elif key == 3:  # Skl_DocsLstSvzM_Docs03 документы перемещение
                    if v['Kol_Spis'] != 0:
                        v['TIPDOC'] = '09'

                # pr_rasx
                if v['TIPDOC'] == '01' or v['TIPDOC'] == '05' or v['TIPDOC'] == '08':
                    pr_rasx = 1
                elif v['TIPDOC'] == '02' or v['TIPDOC'] == '06' or v['TIPDOC'] == '07' or v['TIPDOC'] == '09':
                    pr_rasx = 2
                else:
                    pr_rasx = 3

                # prixod_ou
                if v['TIPDOC'] == '01' or v['TIPDOC'] == '05' or v['TIPDOC'] == '08':
                    prixod_ou = str(v.get("Sum_Skl"))[:-1]
                else:
                    prixod_ou = ln_nul

                # prixod_skl
                if v['TIPDOC'] == '01' or v['TIPDOC'] == '05' or v['TIPDOC'] == '08':
                    prixod_skl = str(v.get("Sum_Prix"))[:-1]
                else:
                    prixod_skl = ln_nul

                # rasxod_ou
                if v['TIPDOC'] == '02' or v['TIPDOC'] == '06' or v['TIPDOC'] == '07' or v['TIPDOC'] == '09':
                    rasxod_ou = str(v.get("Sum_Skl"))[:-1]
                else:
                    rasxod_ou = ln_nul

                # rasxod_skl
                if v['TIPDOC'] == '02' or v['TIPDOC'] == '06' or v['TIPDOC'] == '07' or v['TIPDOC'] == '09':
                    rasxod_skl = str(v.get("Sum_Prix"))[:-1]
                else:
                    rasxod_skl = ln_nul

                # Доп. рассчеты 03
                if v['TIPDOC'] == '03':
                    # prixod_ou
                    if v['yes_membr'] == 1 and v['yes_podr'] == 0:
                        prixod_ou = v['SUMMA']
                    elif v['yes_membr'] == 1 and v['yes_podr'] == 1:
                        prixod_ou = v['SUMMA'] - v['Summa_f']
                    else:
                        prixod_ou = 0

                    # prixod_skl
                    if v['yes_membr'] == 1 and v['yes_podr'] == 0:
                        prixod_skl = v['Sum_Prix']
                    elif v['yes_membr'] == 1 and v['yes_podr'] == 1:
                        prixod_skl = v['SUMMA'] - v['Summa_f']
                    else:
                        prixod_skl = 0

                    # rasxod_ou
                    if v['yes_membr'] == 0 and v['yes_podr'] == 1:
                        rasxod_ou = v['Summa_f']
                    else:
                        rasxod_ou = 0

                    # rasxod_skl
                    if v['yes_membr'] == 0 and v['yes_podr'] == 1:
                        rasxod_skl = v['Sum_rasx']
                    else:
                        rasxod_skl = 0

                    # skl_prix
                    if v['yes_membr'] == 1 and v['yes_podr'] == 1:
                        skl_prix = 0
                    else:
                        skl_prix = v['Skl_Prix']

                # Итоговая таблица
                if v['TIPDOC'] == '01':
                    tipdoc_nm = 'Приход'
                elif v['TIPDOC'] == '02':
                    tipdoc_nm = 'Продажа'
                elif v['TIPDOC'] == '03':
                    tipdoc_nm = 'Перемещение'
                elif v['TIPDOC'] == '05':
                    tipdoc_nm = 'Вз. от покупателя'
                elif v['TIPDOC'] == '06':
                    tipdoc_nm = 'Спиание'
                elif v['TIPDOC'] == '07':
                    tipdoc_nm = 'Возврат потавщику'
                elif v['TIPDOC'] == '08':
                    tipdoc_nm = 'Излики'
                elif v['TIPDOC'] == '09':
                    tipdoc_nm = 'Спиание по перемещени'

                print(prixod_skl)

                self.ws.append(
                    [tipdoc_nm, document, skl_prix, sch_doc, prixod_ou, prixod_skl, rasxod_ou, rasxod_skl, kolvo_skl,
                     kolvo_prix, podr, comment, member_nm])

        print(sum_skl_prixod, sum_skl_rashod)
        self.write_workbook('F12', s_sum_skl)
        self.write_workbook('H12', s_rasxod_skl)
        self.save_excel()


class SqlData(object):
    def __init__(self):
        self.error_sql = ""
        self.conn = ""
        self.result = []


    def sql_exec(self, sql, d):
        try:
            cursor = self.sql_connect()
            if d == "fetchone":
                cursor.execute(sql)
                data = cursor.fetchone()
            elif d == "fetchall":
                cursor.execute(sql)
                data = cursor.fetchall()
            cursor.close()
            self.conn.close()
            return data
        except Exception as e:
            self.error_sql = "".join("%s" % e)

    # сальдо за сутки ОУ
    def select_saldo_ou(self, tip):
        now = datetime.now()
        defname = {0: 'Skl_DocsLstSvzM_Docs01', 1: 'Skl_DocsLstSvzM_Docs02', 2: 'Skl_DocsLstSvzM_Docs',
                   3: 'Skl_DocsLstSvzM_Docs03'}
        i = 0
        query = {}
        for name in defname.values():
            query[i] = self.sql_exec("""exec sp_executesql N'Exec dbo.%s @P1 ,@P2 ,@P3 ,@P4 ,@P5 ',
                                        N'@P1 varchar(2),@P2 datetime2,@P3 datetime2,@P4 varchar(2),@P5 float',
                                        '0R','2018-06-09 00:00:00','2018-06-09 00:00:00','ZZ',%s""" % (name, tip),
                                     "fetchall")
            i = i + 1
        if query:
            return query
        else:
            self.error_sql = "".join("Ошибка получения данных из БД. (select_head)")

    def get_podr_id(self, podr_name):
        query = self.sql_exec("""SELECT
                                    k_member.Member, k_member.Name
                                    from K_Filial, k_member
                                    where Group_ID = 'PODRR' and k_member.Name = '%s'""" % podr_name, "fetchone")
        if query:
            return query['Member']
        else:
            self.error_sql = "".join("Ошибка получения данных из БД. (select_head)")

    def get_data_result(self, tip):
        result_saldo_ou = self.select_saldo_ou(tip)
        CreateExcel().create_excel_data(result_saldo_ou, tip)


SqlData().get_data_result(3)

# TIPDOC - Тип документа
# 03 - Приход
# 02 - Продажа
# 05 - Возврат от
